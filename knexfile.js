// Update with your config settings.

const { mysql } = require("./config");

module.exports = {
  development: {
    client: "mysql2",
    connection: mysql,
    migrations: {
      tableName: "knex_migrations",
    },
  },

  /*
  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },
  */

  production: {
    client: "mysql2",
    connection: mysql,
    migrations: {
      tableName: "knex_migrations",
    },
  },
};
