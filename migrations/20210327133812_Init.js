const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
exports.up = function (knex) {
  return knex.schema
    .createTable("Video", (table) => {
      table.string("id").primary();
      table.string("originalFileName");
      table.string("title").index();
      table.text("description");
      table.string("userId").index();
      table.string("userName");
      table.integer("width");
      table.integer("height");
      table.integer("encodeWidth");
      table.integer("encodeHeight");
      table.integer("videoLength");
      table.string("fileHash");
      table.integer("viewCount");
      table.integer("likeCount").defaultTo(0);
      table.integer("dislikeCount").defaultTo(0);
      table.integer("commentCount").defaultTo(0);
      table.string("visibleStatus").defaultTo("private").index();
      table.boolean("isReady").index();
      table.boolean("isBlocked").index();
      table.text("blockComment").nullable().defaultTo(null);
      table.integer("uploadDate");
      table.integer("readyDate").nullable().defaultTo(null);
      table.integer("publicDate").nullable().defaultTo(null);
      table.string("uploadError").nullable().defaultTo(null);
    })
    .createTable("VideoComment", (table) => {
      table.increments("id");
      table.string("videoId").index();
      table.integer("parentId").nullable().defaultTo(null);
      table.string("userId");
      table.string("userName");
      table.text("comment");
      table.integer("commentCount").defaultTo(0);
      table.integer("likeCount").defaultTo(0);
      table.integer("dislikeCount").defaultTo(0);
      table.integer("insertDate");
      table.integer("lastUpdateDate").nullable().defaultTo(null);
    })
    .createTable("VideoLike", (table) => {
      table.increments("id");
      table.string("videoId").index("videoId");
      table.string("userId");
      table.boolean("type");
      table.integer("insertDate");
    })
    .createTable("VideoTag", (table) => {
      table.increments("id");
      table.string("videoId").index();
      table.string("value");
    })
    .createTable("VideoViewCount", (table) => {
      table.increments("id");
      table.string("videoId").index();
      table.string("ip").nullable().defaultTo(null).index();
      table.string("userId").nullable().defaultTo(null).index();
      table.integer("date");
    })

    .createTable("EncodeQueue", (table) => {
      table.increments("id");
      table.string("videoId");
      table.string("fileName");
      table.integer("resolution");
      table.string("status");
      table.string("progressText");
      table.integer("startDate").nullable().defaultTo(null);
      table.integer("endDate").nullable().defaultTo(null);
      table.integer("insertDate");
    })

    .createTable("User", (table) => {
      table.string("id").primary();
      table.string("name").index();
      table.string("password");
      table.string("email").index();
      table.integer("emailVerified");
      table.string("emailVerifyCode");
      table.integer("registerDate");
      table.boolean("isVirtualUser").defaultTo(false);
    })
    .createTable("UserAttribute", (table) => {
      table.string("userId");
      table.string("attribute").index();
      table.string("value").nullable().defaultTo(null);
      table.integer("insertDate");
    })
    .createTable("UserLoginToken", (table) => {
      table.string("userId");
      table.string("token");
      table.integer("loginDate");
    })

    .createTable("Playlist", (table) => {
      table.string("id").primary();
      table.string("title").index();
      table.text("description");
      table.string("visibleStatus").index();
      table.string("userId").index();
      table.string("userName");
      table.integer("itemCount").defaultTo(0);
      table.integer("createdDate");
      table.integer("lastUpdateDate");
    })
    .createTable("PlaylistItem", (table) => {
      table.increments("id");
      table.string("videoId").index();
      table.string("userId").index();
      table.string("playlistId").index();
      table.integer("sequence");
    })

    .createTable("Subtitle", (table) => {
      table.increments("id");
      table.string("originalFileName");
      table.string("fileHash");
      table.string("videoId").index();
      table.string("title");
      table.text("description");
      table.string("userId").index();
      table.string("userName");
      table.boolean("isMain").index();
      table.integer("upvote").defaultTo(0);
      table.integer("downvote").defaultTo(0);
      table.integer("uploadDate");
    })
    .createTable("SubtitleVote", (table) => {
      table.increments("id");
      table.string("userId");
      table.integer("subtitleId").index();
      table.boolean("type");
      table.integer("insertDate");
    });
};

exports.down = function (knex) {};
