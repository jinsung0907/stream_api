const { Knex } = require("knex");

/**
 *
 * @param {Knex} knex
 */
exports.up = function (knex) {
  return knex.schema.table("EncodeQueue", function (table) {
    table.text("errorMsg");
  });
};

/**
 *
 * @param {Knex} knex
 */
exports.down = function (knex) {
  return knex.schema.table("EncodeQueue", function (table) {
    table.dropColumn("errorMsg");
  });
};
