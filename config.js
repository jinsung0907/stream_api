module.exports = {
  expressPort: 4500,
  mysql: {
    user: "video",
    password: "video",
    database: "video",
  },
  minio: {
    endPoint: process.env.NODE_ENV === "production" ? "" : "localhost",
    port: 7000,
    useSSL: process.env.NODE_ENV === "production",
    accessKey: process.env.NODE_ENV === "production" ? "" : "minioadmin",
    secretKey: process.env.NODE_ENV === "production" ? "" : "minioadmin",
  },
  CDNURL: "http://localhost:7000",
  ipEncKey: {
    key: "samplekey",
    salt: "samplesalt",
  },
  cookieEncKey: "f89a3i&@&eb1od812",
};
