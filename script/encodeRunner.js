const os = require("os");
const path = require("path");
const Queue = require("promise-queue");
const Knex = require("knex");
const { Model } = require("objection");
const EncodeQueue = require("../models/EncodeQueue");
const Video = require("../models/Video");
const { encoding, thumbmail } = require("../lib/ffmpeg");
const { tmpdir } = require("../lib/constants");
const Minio = require("minio");
const MinioClient = require("../lib/Minio");
const walk = require("walk");
const { mysql } = require("../config");

// concurrent encoding size
const maxConcurrent = 4;
const maxQueue = Infinity;
const queue = new Queue(maxConcurrent, maxQueue);

const knex = Knex({
  client: "mysql2",
  connection: mysql,
});

const minio = MinioClient.getClient();

Model.knex(knex);

init();

async function init() {
  /*
    status가 running, queued일 경우 다시 waiting 으로 변경해줌
    현재 encodeRunner를 돌릴 수 있는건 동시에 1개만 되며
    나중에 여러개로 확장할 경우 취소되는 작업들에 대한 처리가 따로 필요함
  */
  await EncodeQueue.query()
    .patch({ status: "waiting" })
    .where("status", "running")
    .orWhere("status", "queued");

    await initMinioBucket();

  // promise queue에 EncodeQueue 넣기
  // 주기 : 1분
  setInterval(async () => {
    // waiting 큐 조회
    const queues = await EncodeQueue.query()
      .select()
      .where("status", "waiting");
    // console.log(queues.length + " waiting record founded");

    for (const i in queues) {
      // 중복으로 큐에 들어가지 않도록 queued 로 값 변경
      await queues[i].$query().patch({ status: "queued" });

      queue.add(() => {
        console.log(
          `Adding job, Total queue : ${queue.getQueueLength()} Pending queue: ${queue.getPendingLength()}`
        );

        return processWaitingQueue(queues[i]);
      });
    }

    /*
  console.log(
    `Total queue : ${queue.getQueueLength()} Pending queue: ${queue.getPendingLength()}`
  );
  */
  }, 5000);
}

/**
 *
 * @param {EncodeQueue} que EncodeQueue model for encoding
 *
 * @returns {boolean} return
 */
async function processWaitingQueue(que) {
  try {
    const vid = await Video.query().findById(que.videoId);

    // 상태 running으로 변경
    await que
      .$query()
      .patch({ status: "running", startDate: Math.floor(Date.now() / 1000) });

    // 인코딩 시작
    const vidPath = path.join(tmpdir, que.fileName);
    const command = await encoding({
      file: vidPath,
      resolution: que.resolution,
      width: vid.width,
      height: vid.height,
      videoId: que.videoId,
    });

    await new Promise((resolve, reject) => {
      // 인코딩 이벤트 처리
      let updateCount = 0;
      command
        // 진행사항
        .on("progress", async (progress) => {
          // 과부하 방지를 위해 10번째마다 진행사항 업데이트
          if (updateCount % 10 === 0) {
            await que.$query().patch({
              progressText: (Math.floor(progress.percent * 10) / 10).toString(),
            });

            // status가 canceled로 바뀌었거나 큐가 삭제되었으면 인코딩 취소
            const status = await que.$query().select("status");
            if (!status || status.status === "canceled") {
              command.kill();
            }
          }
          updateCount++;
        })

        // 에러발생시
        .on("error", async (err, stdout, stderr) => {
          console.error(
            "vid: " + que.videoId + " Cannot process video: " + err.message
          );

          // 인코드 큐에 에러메시지 입력
          await que
            .$query()
            .patch({ status: "errored", errorMsg: err.message });

          // 비디오 테이블에 에러 입력
          await vid
            .$query()
            .patch({ uploadError: "에러발생" })
            .then(() => reject(err));
        })

        // 인코딩 종료시
        .on("end", (output) => {
          resolve();
        });
    });

    // 인코딩 파일 스토리지로 이동
    await uploadFolder(
      minio,
      path.join(tmpdir, que.videoId),
      que.videoId,
      "video"
    );
    // 인코딩 파일 이동 끝

    // 썸네일 추출 기본 썸네일 25%구간에서 추출
    const tnpath = await thumbmail({
      file: vidPath,
      savepath: path.join(os.tmpdir(), vid.id) + ".png",
    });
    // 저장
    await minio.fPutObject("thumbnail", vid.id + ".png", tnpath);
    // 썸네일 끝

    // 큐에 완료 표시 및 완료시각 입력
    await que
      .$query()
      .patch({ status: "completed", endDate: Math.floor(Date.now() / 1000) });

    // 비디오ID로 조회해서 해당 비디오에 대한 작업이 모두 완료되었으면
    // isReady값을 변경해줌
    const result = await vid
      .$relatedQuery("EncodeQueue")
      .whereNot("status", "completed");

    // 완료안된것이 하나라도 있으면 안끝난것
    if (result.length === 0) {
      // 원본 파일 스토리지로 이동
      await minio.fPutObject("videoorigin", que.fileName, vidPath);
      // 원본 파일 이동 끝
      
      const patch = {
        isReady: true,
        readyDate: Math.floor(Date.now() / 1000),
      };

      // 업로드시 공개설정을 했으면 공개시각도 입력
      if (vid.visibleStatus === "public") {
        patch.publicDate = Math.floor(Date.now() / 1000);
      }

      await vid.$query().patch(patch);
    }

    return true;
  } catch (e) {
    console.error(e);
    throw e;
  }
}

/**
 * @param {Minio.Client} client Minio client
 * @param {string} path path to upload
 * @param {string} targetPath destination path
 * @param {string} bucket bucket name
 */

async function uploadFolder(client, sourcePath, targetPath, bucket) {
  return new Promise((resolve, reject) => {
    const walker = walk.walk(sourcePath);
    walker.on("file", (root, fileStats, next) => {
      const filePath = path.join(root, fileStats.name);
      client.fPutObject(
        bucket,
        targetPath + "/" + fileStats.name,
        path.join(filePath),
        (err) => {
          if (err) {
            reject(err);
          }
          next();
        }
      );
    });

    walker.on("end", function () {
      console.log("End upload");
      resolve();
    });
  });
}

async function initMinioBucket() {
  console.log("video", await minio.bucketExists("video"));
  console.log("videoorigin", await minio.bucketExists("videoorigin"));
  console.log("thumbnail", await minio.bucketExists("thumbnail"));
  console.log("subtitle", await minio.bucketExists("subtitle"));
  console.log("subtitleoriginal", await minio.bucketExists("subtitleoriginal"));
  
  if (!(await minio.bucketExists("video"))) {
    await minio.makeBucket("video");
  }

  if (!(await minio.bucketExists("videoorigin"))) {
    await minio.makeBucket("videoorigin");
  }

  if (!(await minio.bucketExists("thumbnail"))) {
    await minio.makeBucket("thumbnail");
  }

  if (!(await minio.bucketExists("subtitle"))) {
    await minio.makeBucket("subtitle");
  }

  if (!(await minio.bucketExists("subtitleoriginal"))) {
    await minio.makeBucket("subtitleoriginal");
  }

  await minio.setBucketPolicy("video", `
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "s3:GetObject",
        "Effect": "Allow",
        "Principal": {"AWS": "*"},
        "Resource": ["arn:aws:s3:::video/*"],
        "Sid": "Public"
      }
    ]
  }
  `);
  // await minio.setBucketPolicy("videoorigin", "public");
  await minio.setBucketPolicy("thumbnail", `
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "s3:GetObject",
        "Effect": "Allow",
        "Principal": {"AWS": "*"},
        "Resource": ["arn:aws:s3:::thumbnail/*"],
        "Sid": "Public"
      }
    ]
  }
`);
  await minio.setBucketPolicy("subtitle", `
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "s3:GetObject",
        "Effect": "Allow",
        "Principal": {"AWS": "*"},
        "Resource": ["arn:aws:s3:::subtitle/*"],
        "Sid": "Public"
      }
    ]
  }
`);
  // await minio.setBucketPolicy("subtitleoriginal", "public");
}
