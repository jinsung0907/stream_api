const os = require("os");
const express = require("express");
const fileUpload = require("express-fileupload");
const app = express();
const session = require("express-session");
const Knex = require("knex");
const { Model } = require("objection");
const router = require("./router/index.js");
const UserLoginToken = require("./models/UserLoginToken.js");
const User = require("./models/User.js");
const { mysql, cookieEncKey, expressPort } = require("./config.js");
const { checkConfigFile } = require("./lib/common.js");

checkConfigFile();

const knex = Knex({
  client: "mysql2",
  connection: {
    ...mysql,
    typeCast: (field, next) => {
      // console.log("TypeCasting", field.type, field.length);
      if (field.type === "TINY" && field.length === 1) {
        const value = field.string();
        return value ? value === "1" : null;
      }
      return next();
    },
  },
});

Model.knex(knex);

app.listen(expressPort, () => {
  console.log(
    "server run in " + process.env.NODE_ENV + " mode on port " + expressPort
  );
});

app.disable("x-powered-by");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  fileUpload({
    useTempFiles: true,
    tempFileDir: os.tmpdir(),
  })
);

app.use(
  session({
    secret: cookieEncKey,
    resave: false,
    saveUninitialized: true,
  })
);

// CORS
app.use(async (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "Content-type, authorization");
  next();
});

// 세션 로그인 상태 체크하여 맞으면 유저 모델 불러옴
app.use(async (req, res, next) => {
  try {
    // 로그인 세션이 없고, 토큰이 설정되어있으면 토큰로그인
    if (typeof req.session.userid === "undefined") {
      let token = req.header("Authorization");
      if (typeof token !== "undefined") {
        token = token.replace("Bearer ", "");

        const logintoken = await UserLoginToken.query()
          .select()
          .where("token", token);

        if (typeof logintoken !== "undefined" && logintoken.length !== 0) {
          req.session.userid = logintoken[0].userId;
          req.user = await logintoken[0].$relatedQuery("User");
          // req.user = await logintoken[0].$relatedQuery("User").findById();
        }
      }
    } else {
      req.user = await User.query().findById(req.session.userid);
    }
    next();
  } catch (e) {
    console.error(e);
  }
});

app.use(router);

app.get("/", (req, res) => {
  res.send("^오^ <a href='/login'>as</a>");
});

app.get("/version", (req, res) => {
  const packageDetails = require("./package.json");

  res.send(packageDetails.version);
});

app.options("*", (req, res) => res.status(200));
