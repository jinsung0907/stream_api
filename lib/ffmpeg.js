const ffmpeg = require("fluent-ffmpeg");
const { tmpdir } = require("./constants");
const ffprobe = ffmpeg.ffprobe;
const fs = require("fs");
const path = require("path");

module.exports.probe = (file) =>
  new Promise((resolve, reject) => {
    ffprobe(file, (err, data) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(data);
    });
  });

/**
 *
 * @param {{
 *  file: string,
 *  resolution: number,
 *  width: number,
 *  height: number,
 *  videoId: string}} param encode Options
 *
 * @returns {ffmpeg.FfmpegCommand} ffmpegcommand
 */
module.exports.encoding = ({ file, resolution, width, height, videoId }) => {
  if (
    typeof file === "undefined" ||
    typeof resolution === "undefined" ||
    typeof width === "undefined" ||
    typeof height === "undefined" ||
    typeof videoId === "undefined"
  ) {
    throw new Error("invalid params");
  }

  // 폴더가 없으면 생성해줌
  if (!fs.existsSync(path.join(tmpdir, videoId))) {
    fs.mkdirSync(path.join(tmpdir, videoId), { recursive: true });
  }

  // ffmpeg 명령 실행
  const command = ffmpeg(file)
    .videoCodec("libx264") // h264
    .audioCodec("aac") // aac 128k
    .audioBitrate(128);

  if (width > height) {
    command.size(`?x${resolution}`);
  } else {
    command.size(`${resolution}x?`);
  }

  command
    .addOptions([
      //    "-tune animation",
      "-hls_time 5", // hls chunk 길이 (5초)
      "-hls_allow_cache 1", // 캐시 허용
      "-hls_playlist_type vod", // vod 타입
      `-hls_segment_filename ${tmpdir}/${videoId}/${videoId}_${resolution}_%d.ts`,
    ])
    .format("hls")
    .save(`${tmpdir}/${videoId}/${videoId}_${resolution}.m3u8`);

  /*
    .on("start", (command) => {
      // console.log(command);
    })
    .on("progress", (progress) => {
      // console.log(progress)
      // console.log(progress.frames + '/' + frames)
      console.log((progress.frames / frames) * 100);
    })
    .on("error", function (err, stdout, stderr) {
      console.log("Cannot process video: " + err.message);
    })
    .on("end", (output) => {
      console.log(output);
    });
    */

  return command;
};

module.exports.thumbmail = async ({ file, savepath }) =>
  new Promise((resolve, reject) => {
    ffmpeg(file)
      .screenshot({
        timemarks: ["25%"],
        filename: savepath,
      })

      .on("end", (output) => {
        resolve(savepath);
      });
  });
