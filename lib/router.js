const CustomError = require("./CustomError");

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @param {CustomError | Error} e
 */
module.exports.handleRouterError = (req, res, e) => {
  if (e instanceof CustomError) {
    res.status(e.status).json({
      errorMsg: e.message,
    });
  } else {
    console.error(e);
    res.status(500).json({ errorMsg: "Generic Error" });
  }
};
