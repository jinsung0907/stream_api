const fs = require("fs").promises;
const fileUpload = require("express-fileupload");
const Subtitle = require("../models/Subtitle");
const User = require("../models/User");
const Video = require("../models/Video");
const MinioClient = require("./Minio");
const subsrt = require("subsrt");
const { getTimeStamp } = require("./common");
const jschardet = require("jschardet");
const iconv = require("iconv-lite");
const path = require("path");

/**
 *
 * @param {{ video: Video, user: User, file: fileUpload.UploadedFile, metadata: object}} param0
 * @param trx
 *
 * @returns {Subtitle} subtitle model
 */
module.exports.uploadSubtitle = async (
  { video, user, file, metadata },
  trx
) => {
  if (!(video instanceof Video)) {
    throw new Error("invalid video model");
  }
  if (!(user instanceof User)) {
    throw new Error("invalid user model");
  }
  if (typeof file === "undefined") {
    throw new Error("invalid file upload");
  }
  if (typeof metadata === "undefined") {
    throw new Error("invalid metadata");
  }

  try {
    const subtitle = await Subtitle.query(trx).insertAndFetch({
      originalFileName: file.name,
      fileHash: file.md5,
      videoId: video.id,
      title: metadata.title,
      description: metadata.description,
      userId: user.id,
      userName: user.name,
      isMain: false,
      uploadDate: getTimeStamp(),
    });

    // 파일 불러옴
    const buffer = await fs.readFile(file.tempFilePath);

    // 인코딩 확인
    let encoding = jschardet.detect(buffer);
    if (Array.isArray(encoding)) {
      encoding = encoding[0];
    }
    const sub = iconv.decode(buffer, encoding.encoding);

    let vtt = sub;
    if (path.extname(file.name) !== ".vtt") {
      // 파일형식이 VTT가 아니면 vtt로 변환
      vtt = subsrt.convert(sub, { format: "vtt" });
    }

    // 스토리지에 저장
    const minio = MinioClient.getClient();
    await minio.fPutObject("subtitleoriginal", file.md5, file.tempFilePath);
    await minio.putObject("subtitle", subtitle.id + ".vtt", vtt);

    return subtitle;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

/**
 *
 * @param {Subtitle} subtitle
 *
 */
module.exports.deleteSubtitleFile = async (subtitle) => {
  const minio = MinioClient.getClient();
  await minio.removeObject("subtitle", subtitle.id + ".vtt");
  await minio.removeObject("subtitleoriginal", subtitle.fileHash);
};
