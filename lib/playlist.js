const Playlist = require("../models/PlayList");
const User = require("../models/User");
const Video = require("../models/Video");
const { randomAlphanumeric, getTimeStamp } = require("./common");

/**
 *
 * @param {string} playlistId playlist id
 * @param {User} user user model
 *
 * @returns {(Playlist|object)} playlist on success, null if not exists
 */
module.exports.playlistById = async (playlistId, user) => {
  if (typeof playlistId === "undefined") {
    throw new Error("invalid playlist id");
  }

  try {
    const playlist = await Playlist.query().findById(playlistId);

    if (typeof playlist === "undefined" || playlist === null) {
      return null;
    }

    // 유저와 공개여부 확인하여 출력
    // 관리자이거나 업로더일경우 비공개 영상 시청 가능
    if (
      playlist.visibleStatus === "private" &&
      (typeof user === "undefined" ||
        (!user.isAdmin() && user.id !== playlist.userId))
    ) {
      return null;
    }

    return playlist;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

/**
 *
 * @param {{ title: string, description: string, visibleStatus: string}} param0
 * @param {Array<Video>} videos
 * @param {User} user
 * @param {*} trx
 * @returns {Playlist} created Playlist model
 */
module.exports.createPlayList = async (
  { title, description, visibleStatus },
  videos,
  user,
  trx
) => {
  try {
    const inserted = await Playlist.query(trx).insertAndFetch({
      id: randomAlphanumeric(),
      title: title,
      description: description,
      visibleStatus: visibleStatus,
      userId: user.id,
      userName: user.name,
      createdDate: getTimeStamp(),
    });

    if (typeof videos !== "undefined") {
      const items = videos.map((val, i) => {
        return {
          videoId: val.id,
          userId: user.id,
          playlistId: inserted.id,
          sequence: i + 1,
        };
      });
      await inserted.$relatedQuery("PlayListItem", trx).insert(items);
    }
    return inserted;
  } catch (e) {
    console.error(e);
    throw e;
  }
};
