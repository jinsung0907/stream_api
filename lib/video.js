const { raw } = require("objection");
const User = require("../models/User");
const Video = require("../models/video");
const VideoComment = require("../models/VideoComment");
const MinioClient = require("./Minio");
const path = require("path");

/**
 * @description update video information
 * @param {Video} video video model to update
 * @param {object} info info for update
 * @returns {Video} Updated Video model
 */
module.exports.updateVideoInfo = async (video, info) => {
  if (!(video instanceof Video)) {
    throw new Error("invalid video model");
  }
  if (typeof info !== "object") {
    throw new Error("invalid video info json");
  }

  try {
    const patch = {
      title: info.title,
      description: info.description,
      // visibleStatus
    };

    // 영상 비공개에서 공개로 바꿀시 공개날짜 입력
    if (video.publicDate === null && info.visibleStatus === "public") {
      patch.publicDate = Math.floor(Date.now() / 1000);
    }

    // 태그 업데이트
    await video.$relatedQuery("VideoTag").delete();
    const tags = info.tags.map((val) => {
      return {
        videoId: video.id,
        value: val,
      };
    });
    await video.$relatedQuery("VideoTag").insert(tags);

    // 정보 업데이트
    const vid = await video.$query().patchAndFetch(patch);

    return vid;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

/**
 *
 * @param {string} videoId videoId
 * @param {user} user user model

 * @returns {Video} video on success, error on object
 */
module.exports.getVideoById = async (videoId, user) => {
  if (typeof videoId === "undefined") {
    throw new Error("invalid VideoId");
  }

  try {
    const video = await Video.query().findById(videoId);

    if (typeof video === "undefined" || video === null) {
      return { status: "notExists" };
    }

    // 유저와 공개여부 확인하여 출력
    // 관리자이거나 업로더일경우 비공개 영상 시청 가능
    if (
      video.visibleStatus === "private" &&
      (typeof user === "undefined" ||
        (!user.isAdmin() && user.id !== video.userId))
    ) {
      return { status: "private" };
    }

    // 관리자만 차단영상 시청 가능
    if (
      video.isBlocked === true &&
      (typeof user === "undefined" || !user.isAdmin())
    ) {
      return { status: "blocked" };
    }

    return video;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

/**
 *
 * @param {Video} video video Model
 * @param {User} user user model
 * @param {{ data: string, parentId: (undefined | string) }} comment comment request data
 *
 * @returns {string | boolean} return true if success, if not return string with reason
 */
module.exports.insertVideoComment = async (video, user, comment) => {
  if (!(video instanceof Video)) {
    throw new Error("invalid video model");
  }
  if (typeof comment === "undefined") {
    throw new Error("invalid comment data");
  }

  try {
    // 권한 체크
    if (!video.commentAllowed()) {
      return "댓글을 달 수 없는 동영상입니다.";
    }

    await Video.transaction(async (trx) => {
      const insertdata = {
        comment: comment.data,
        userId: user.id,
        insertDate: Math.floor(Date.now() / 1000),
      };
      if (typeof comment.parentId !== "undefined") {
        insertdata.parentId = comment.parentId;
      }

      await video.$relatedQuery("VideoComment", trx).insert(insertdata);
      await video.$query(trx).patch({
        commentCount: raw("commentCount + 1"),
      });
    });

    return true;
  } catch (e) {
    console.error(e);
    throw e;
  }
};

/**
 *
 * @param {string} commentId commentid for delete
 * @param {User} user user model
 *
 * @returns {(true|string)} true if success, else reason string
 */
module.exports.deleteVideoComment = async (commentId, user) => {
  if (typeof commentId === "undefined") {
    throw new Error("invalid commentId");
  }
  if (!(user instanceof User)) {
    throw new Error("invalid user model");
  }

  try {
    return await VideoComment.transaction(async (trx) => {
      const comment = await VideoComment.query(trx).findById(commentId);

      if (user.id !== comment.userId) {
        return "자신의 댓글만 삭제 가능합니다.";
      }

      await comment.$relatedQuery("Video", trx).patch({
        commentCount: raw("commentCount - 1"),
      });
      await comment.$query().delete();
      return true;
    });
  } catch (e) {
    console.error(e);
    throw e;
  }
};

module.exports.deleteVideoFiles = async (video) => {
  try {
    const minio = MinioClient.getClient();

    // 원본파일 삭제
    minio.removeObject(
      "videoorigin",
      video.id + path.extname(video.originalFileName)
    );

    // 인코딩 파일 삭제
    await MinioClient.removeFolder("video", `${video.id}/`);

    // 썸네일 삭제
    minio.removeObject("thumbnail", `${video.id}.png`);
  } catch (e) {
    console.error(e);
    throw e;
  }
};
