const User = require("../models/User");
const { getTimeStamp, randomAlphanumeric } = require("./common");
const crypto = require("crypto");
const { nanoid } = require("nanoid");
const CustomError = require("./CustomError");

module.exports.isLogined = (req) => {
  if (typeof req.user === "undefined") {
    return false;
  }

  if (req.user instanceof User) {
    return true;
  }
};

/**
 *
 * @param {{ email: string, password: string, name: string, isVirtual: boolean }} param0
 * @returns {(string | true )} True if success, string if error with reason
 */
module.exports.createUser = async ({ email, password, name, isVirtual }) => {
  if (
    typeof email === "undefined" ||
    typeof password === "undefined" ||
    typeof name === "undefined"
  ) {
    throw new Error("invalid params");
  }

  if (!this.nameValidate(name)) {
    throw new CustomError("invalidname", 400, "닉네임 규칙에 맞춰주세요.");
  }
  if (!this.passwordValidate(password)) {
    throw new CustomError(
      "invalidpassword",
      400,
      "비밀번호 규칙에 맞춰주세요."
    );
  }
  if (!this.emailValidate(email)) {
    throw new CustomError("invalidemail", 400, "유효한 이메일을 입력해주세요.");
  }

  if (await this.isNameDuplicated(name)) {
    throw new CustomError("namedup", 400, "닉네임 중복.");
  }
  if (await this.isEmailDuplicated(email)) {
    throw new CustomError("emaildup", 400, "이메일 중복.");
  }

  const hashInfo = await this.encryptToHash(password);

  const params = {
    id: await this.generateUserId(),
    name: name,
    password: hashInfo.DBFormat,
    email: email,
    emailVerifyCode: await this.generateVerifyCode(),
    registerDate: getTimeStamp(),
    isVirtualUser: isVirtual === true,
  };

  await User.query().insert(params);

  return true;
};

module.exports.nameValidate = (name) => {
  if (typeof name === "undefined") {
    throw new Error("invalid param");
  }

  if (name.length > 12) {
    return false;
  }
  if (!/^[가-힣a-zA-Z0-9 ]+$/.test(name)) {
    return false;
  }

  return true;
};

module.exports.passwordValidate = (password) => {
  if (typeof password === "undefined" || password === "") {
    return false;
  }

  // 패스워드 길이는 6자이상
  if (password.length < 6) {
    return false;
  }

  return true;
};

module.exports.emailValidate = (email) => {
  if (typeof email === "undefined") {
    return false;
  }

  return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
    email
  );
};

module.exports.isNameDuplicated = async (name) => {
  if (typeof name === "undefined") {
    throw new Error("invalid param");
  }
  try {
    const list = await User.query().select().where({
      name: name,
    });

    if (list.length === 0) {
      return false;
    } else {
      return true;
    }
  } catch (e) {
    console.error(e);
    throw e;
  }
};

module.exports.isEmailDuplicated = async (email) => {
  if (typeof email === "undefined") {
    throw new Error("invalid param");
  }
  try {
    const list = await User.query().select().where({
      email: email,
    });

    if (list.length === 0) {
      return false;
    } else {
      return true;
    }
  } catch (e) {
    console.error(e);
    throw e;
  }
};

module.exports.isVerifyCodeDuplicated = async (value) => {
  if (typeof value === "undefined") {
    throw new Error("invalid param");
  }

  try {
    const list = await User.query().select().where({
      emailVerifyCode: value,
    });

    if (list.length === 0) {
      return false;
    } else {
      return true;
    }
  } catch (e) {
    console.error(e);
    throw e;
  }
};

module.exports.generateVerifyCode = async () => {
  try {
    const code = nanoid();

    if (await this.isVerifyCodeDuplicated(code)) {
      return this.generateVerifyCode();
    } else {
      return code;
    }
  } catch (e) {
    console.error(e);
    throw e;
  }
};

module.exports.generateUserId = async (trx) => {
  const userid = randomAlphanumeric(7);

  const res = await User.query(trx).findById(userid);

  if (typeof res === "undefined") {
    return userid;
  } else {
    return await this.generateUserId(trx);
  }
};

/**
 *
 * @param {string} string
 * @returns {{value: string, hash: string, salt: string, cost: number, keylength: number, algorithm: string, DBFormat: string}}
 */
module.exports.encryptToHash = (string) =>
  new Promise((resolve, reject) => {
    crypto.randomBytes(64, (err, buf) => {
      if (err) {
        console.error(err);
        reject(new Error("random byte generate error"));
      }
      const cost = 30000;
      const keylength = 64;
      const algorithm = "sha512";

      const salt = buf.toString("base64");
      crypto.pbkdf2(string, salt, cost, keylength, algorithm, (err, key) => {
        if (err) {
          console.error(err);
          reject(new Error("encrypt to hash error"));
        }
        const hash = key.toString("base64");

        resolve({
          value: string,
          hash: hash,
          salt: salt,
          cost: cost,
          keylength: keylength,
          algorithm: algorithm,
          DBFormat: [algorithm, keylength, cost, salt, hash].join(":"),
        });
      });
    });
  });

module.exports.checkHash = ({
  value,
  hash,
  salt,
  algorithm,
  cost,
  keylength,
}) =>
  new Promise((resolve, reject) => {
    crypto.pbkdf2(value, salt, cost, keylength, algorithm, (err, key) => {
      if (err) {
        console.error(err);
        reject(new Error("error while create password hash"));
      }
      if (key.toString("base64") === hash) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  });
