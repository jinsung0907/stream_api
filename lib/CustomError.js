module.exports = class CustomError extends Error {
  /**
   *
   * @param {ERROR_CODE} code
   * @param {HTTP STATUS CODE} status
   * @param  {...any} params
   */
  constructor(code = "GENERIC", status = 500, ...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError);
    }

    this.code = code;
    this.status = status;
  }
};
