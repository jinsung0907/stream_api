const Video = require("../models/Video");

module.exports.searchVideoByString = async (searchstr) => {
  if (typeof searchstr === "undefined") {
    throw new Error("invalid search string");
  }

  try {
    const videos = await Video.query()
      .where("title", "like", `%${searchstr}%`)
      .orWhere("description", "like", `%${searchstr}%`)
      .where("visibleStatus", "public")
      .where("isReady", true)
      .whereNot("isBlocked", true);

    return videos;
  } catch (e) {
    console.error(e);
    throw e;
  }
};
