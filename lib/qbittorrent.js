const bent = require("bent");

class TorrentClient {
  static async Connect(force) {
    if (this.SID !== null && typeof force !== "undefined") {
      return;
    }

    const stream = await this.bent(
      `/api/v2/auth/login?username=${this.username}&password=${this.password}`
    );

    if (stream.statusCode !== 200) {
      throw new Error("invalid status code : " + stream.statusCode);
    }

    if ((await stream.text()) === "Fails.") {
      throw new Error("invalid user credential");
    }

    this.SID = stream.headers["set-cookie"][0]
      .split(";")[0]
      .replace("SID=", "");

    this.options = { Cookie: "SID=" + this.SID + ";" };
    return this;
  }

  static async getTorrentList() {
    const stream = await this.bent(
      "/api/v2/torrents/info",
      "GET",
      this.options
    );

    return await stream.json();
  }

  static async deleteTorrent(hash, deleteFile) {
    if (!Array.isArray(hash)) {
      hash = [hash];
    }
    const params = new URLSearchParams();
    params.append("hashes", hash.join("|"));
    params.append("deleteFiles", deleteFile ? "true" : "false");

    const stream = await this.bent(
      "/api/v2/torrents/delete" + params.toString(),
      "GET",
      this.options
    );

    if (stream.statusCode === 200) {
      return true;
    }
  }

  static async addFeed(url) {
    const params = new URLSearchParams();
    params.append("url", url);

    const stream = await this.bent(
      "/api/v2/rss/addFeed" + params.toString(),
      "GET",
      this.options
    );

    if (stream.statusCode === 200) {
      return true;
    }
  }
}

TorrentClient.username = "";
TorrentClient.password = "";
TorrentClient.url = "http://localhost:8080";
TorrentClient.bent = bent(TorrentClient.url);
TorrentClient.SID = null;
TorrentClient.options = {};

module.exports = TorrentClient;
