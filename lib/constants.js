const os = require("os");
const path = require("path");

const vars = {
  tmpdir: path.join(os.tmpdir(), "htv"),
  VIEWCOUNT_CHECK_TIME: 12 * 60 * 60,
  COMMENT_PAGING: 30,
};

module.exports = vars;
