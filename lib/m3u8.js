const m3u8 = require("m3u8");
const { CDNURL } = require("../config");
const { getTargetResolution } = require("./upload");

module.exports.createMasterPlaylist = (video) => {
  const res = getTargetResolution(video.width, video.height);

  const m3u = m3u8.M3U.create();

  res.map((val) => {
    m3u.addStreamItem({
      resolution: `${getWidth(val)}x${val}`,
      bandwidth: getBandwidth(val),
      uri: `${CDNURL}/video/${video.id}/${video.id}_${val}.m3u8`,
    });
  });

  return m3u.toString();
};

const getWidth = (height) => {
  switch (height) {
    case 480:
      return 842;
    case 720:
      return 1280;
    case 1080:
      return 1920;
  }
};

const getBandwidth = (height) => {
  switch (height) {
    case 480:
      return 1400000;
    case 720:
      return 2800000;
    case 1080:
      return 5000000;
  }
};
/*
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-STREAM-INF:BANDWIDTH=800000,RESOLUTION=640x360
360p.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=1400000,RESOLUTION=842x480
480p.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=2800000,RESOLUTION=1280x720
720p.m3u8
#EXT-X-STREAM-INF:BANDWIDTH=5000000,RESOLUTION=1920x1080
1080p.m3u8
*/
