module.exports.getTimeStamp = () => {
  return Math.floor(Date.now() / 1000);
};

module.exports.getIpAddress = (req) => {
  if (req.headers["cf-connecting-ip"]) {
    return req.headers["cf-connecting-ip"];
  } else if (req.headers["x-forwarded-for"]) {
    const arr = req.headers["x-forwarded-for"].split(":");
    return arr[0];
  } else if (req.headers["X-Forwarded-For"]) {
    const arr = req.headers["X-Forwarded-For"].split(":");
    return arr[0];
  } else {
    return req.ip;
  }
};

module.exports.randomAlphanumeric = (size) => {
  if (typeof size === "undefined") size = 6;
  const { customAlphabet } = require("nanoid");

  const randid = customAlphabet(
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIKJLMNOPQRSTUVWXYZ",
    size
  );

  return randid();
};

module.exports.isValidVisibleStatus = (str) => {
  switch (str) {
    case "public":
    case "unlisted":
    case "private":
      return true;
    default:
      return false;
  }
};

module.exports.checkConfigFile = () => {
  try {
    const config = require("../config");

    if (!config.expressPort) {
      throw new Error("Set expressPort");
    }
    if (!config.mysql) {
      throw new Error("Set mysql");
    }
    if (!config.minio) {
      throw new Error("Set Minio");
    }
    if (!config.CDNURL) {
      throw new Error("Set CDNURL");
    }
    if (!config.ipEncKey) {
      throw new Error("Set IP Encryption Key");
    }
    if (!config.cookieEncKey) {
      throw new Error("Set Cookie Encryption Key");
    }
    console.log("Config file check done.");
  } catch (e) {
    console.error(e);
    if (e.code === "MODULE_NOT_FOUND") {
      console.log("no config file");
    }
    process.exit(1);
  }
};
