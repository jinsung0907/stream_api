const { tmpdir } = require("./constants");
const path = require("path");
const Video = require("../models/Video");
const EncodeQueue = require("../models/EncodeQueue");
const { probe } = require("./ffmpeg");
const fileUpload = require("express-fileupload");
const { randomAlphanumeric } = require("./common");
const fs = require("fs").promises;

/**
 *
 * @param {{ file: fileUpload.UploadedFile, info: object}} param0 params
 * @returns {Video}
 */
module.exports.uploadVideo = async ({ file, info, user }) => {
  try {
    this.uploadJsonValidate(info);

    // 트랜젝션 시작
    const result = await Video.transaction(async (trx) => {
      // id중복 확인
      while (1) {
        const id = randomAlphanumeric();
        const result = await Video.query(trx).findById(id);
        if (typeof result === "undefined") {
          info.id = id;
          break;
        }
      }
      info.originalFileName = file.name;

      const value = {
        id: info.id,
        originalFileName: file.name,
        fileHash: file.md5,
        title: info.title,
        description: info.description,
        userId: user.id,
        userName: user.name,
        visibleStatus: info.visible,
        isReady: false,
        isBlocked: false,
        uploadDate: Math.floor(Date.now() / 1000),
        readyDate: null,
      };

      // 파일 저장
      await fs.mkdir(tmpdir, { recursive: true });
      await file.mv(path.join(tmpdir, `${info.id}${path.extname(file.name)}`));

      // 해상도 저장
      const metadata = await probe(
        `${tmpdir}/${info.id}${path.extname(file.name)}`
      );
      for (const i in metadata.streams) {
        const stream = metadata.streams[i];

        if (stream.codec_type === "video") {
          value.width = stream.width;
          value.height = stream.height;
          break;
        }
      }

      // 영상 길이 저장
      value.videoLength = Math.floor(metadata.format.duration);

      // DB 입력
      const result = await Video.query(trx).insert(value);

      // 해상도 별로 인코딩 큐에 입력 (480p, 720p, 1080p)
      const resolutions = this.getTargetResolution(value.width, value.height);
      const encode = {
        videoId: value.id,
        fileName: info.id + path.extname(file.name),
        resolution: 480,
        status: "waiting",
        insertDate: Math.floor(Date.now() / 1000),
      };

      for (const i in resolutions) {
        encode.resolution = resolutions[i];
        await EncodeQueue.query(trx).insert(encode);
      }
      // 해상도별 입력 끝

      return result;
    });
    // 트랜젝션 끝

    return result;
  } catch (e) {
    console.error(e);
    throw new Error("error while upload");
  }
};

module.exports.uploadJsonValidate = (json) => {
  if (typeof json === "undefined") {
    throw new Error("invalid info");
  }
  if (typeof json.title === "undefined") {
    throw new Error("invalid title");
  }
  if (typeof json.description === "undefined") {
    throw new Error("invalid description");
  }
  if (typeof json.visible === "undefined") {
    throw new Error("invalid visible value");
  } else {
    switch (json.visible) {
      case "public":
      case "invisible":
      case "private":
        break;
      default:
        throw new Error("invalid visible value");
    }
  }

  return true;
};

module.exports.getTargetResolution = (width, height) => {
  if (typeof width === "undefined" || typeof height === "undefined") {
    throw new Error("invalid value");
  }

  /**
   * 1080p보다 크면 1080p 고정
   * 720~1080 사이면 720p 고정
   * 480~720 사이면 해당값이 최고값
   * 480 이하면 그 값이 최고값
   */

  const targetRes = Math.max(width, height);

  if (targetRes >= 1080) {
    return [480, 720, 1080];
  } else if (targetRes >= 720 && targetRes < 1080) {
    return [480, 720];
  } else if (targetRes >= 480 && targetRes < 720) {
    return [480, targetRes];
  } else {
    return [targetRes];
  }
};
