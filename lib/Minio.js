const Minio = require("minio");
const { val } = require("objection");
const { minio } = require("../config");

class MinioClient {
  /**
   *
   * @returns {Minio.Client} minio client
   */
  static getClient() {
    if (this.client) {
      return this.client;
    }

    this.client = new Minio.Client(this.option);
    return this.client;
  }

  /**
   *
   * @param {*} stream
   * @param {string} bucket
   * @param {string} folder
   */
  static async removeFolder(bucket, folder) {
    const objects = await new Promise((resolve, reject) => {
      const objects = [];
      const stream = this.client.listObjectsV2(bucket, folder, true);

      stream.on("data", (obj) => {
        objects.push(obj);
      });

      stream.on("error", (err) => {
        reject(err);
      });

      stream.on("end", () => {
        resolve(objects);
      });
    });

    if (objects.length !== 0) {
      this.client.removeObjects(
        bucket,
        objects.map((val) => val.name)
      );

      this.client.removeObject(bucket, folder);
    }
  }
}

MinioClient.client = null;
MinioClient.option = { ...minio };

module.exports = MinioClient;
