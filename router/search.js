const { handleRouterError } = require("../lib/router");
const { searchVideoByString } = require("../lib/search");
const { isLogined } = require("../lib/User");
const Video = require("../models/Video");

/**
 *
 *
 * @param {{body: {videoId: string}, user: User}} req express request object
 * @param {*} res express response object
 * @returns {*} return
 */
module.exports.searchVideo = async (req, res) => {
  try {
    const videos = await searchVideoByString(req.body.searchstr);

    // 태그 가져오기 (아직 안씀)
    // await video.getTags();

    let result = [];
    result = videos.map((val) => {
      return val.toPublicJSON();
    });

    res.json({ status: "success", data: result });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};
