const { isLogined } = require("../lib/User");
const { getVideoById, deleteVideoComment } = require("../lib/video");
const Video = require("../models/Video");
const User = require("../models/User");
const { getIpAddress } = require("../lib/common");
const { COMMENT_PAGING } = require("../lib/constants");
const { createMasterPlaylist } = require("../lib/m3u8");
const { handleRouterError } = require("../lib/router");

/**
 *
 * @param {{body: {videoId: string}, user: User}} req express request object
 * @param {*} res express response object
 * @returns {*} return
 */
module.exports.videoById = async (req, res) => {
  try {
    const video = await getVideoById(req.params.id, req.user);

    if (video instanceof Video) {
      if (video.status === "notExists") {
        res.status(404);
      } else if (video.status === "private") {
        res.status(403);
      } else if (video.status === "blocked") {
        res.status(403);
      }
      res.json(video);
      return;
    }

    // 태그 가져오기
    await video.getTags();

    // 권한에 따라 리턴
    let json;
    if (isLogined(req)) {
      if (req.user.isAdmin()) {
        json = video.toJSON();
      } else if (req.user.id === video.userId) {
        json = video.toOwnerJSON();
      }
    } else {
      json = video.toPublicJSON();
    }
    res.json({ status: "success", data: json });
  } catch (e) {
    console.error(e);
    res.status(500).end();
    res.json({ errorMsg: "err" });
  }
};

/**
 *
 * @param {{ body: { paging: number } }} req express request object
 * @param {*} res res
 */
module.exports.listVideo = async (req, res) => {
  let paging = req.params.paging;
  if (typeof req.params.paging === "undefined" || req.params.paging <= 0) {
    paging = 1;
  }

  try {
    const videos = await Video.query()
      .where({
        visibleStatus: "public",
        isReady: true,
        isBlocked: false,
      })
      .limit(20)
      .offset((paging - 1) * 20);

    const jsons = videos.map((val) => val.toPublicJSON());

    res.json({ status: "success", data: jsons });
  } catch (e) {
    console.error(e);
    res.status(500).end();
    res.json({ errorMsg: "err" });
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.viewCountVideo = async (req, res) => {
  if (typeof req.params.id === "undefined") {
    res.status(403).json({ errorMsg: "invalid param" });
    return;
  }
  try {
    const videoId = req.params.id;

    const video = await Video.query().findById(videoId);

    if (!video) {
      res.status(404).json({ status: "novideo" });
      return;
    }

    await video.viewCountUp({
      ip: getIpAddress(req),
      user: req.user,
    });

    res.json({ status: "success" });
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.likeVote = async (req, res) => {
  if (
    typeof req.body.videoId === "undefined" ||
    typeof req.body.type === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }

  try {
    const video = await Video.query().findById(req.body.videoId);

    let data;
    if (req.body.type === true) {
      data = await video.upLike(req.user);
    } else {
      data = await video.upDislike(req.user);
    }

    res.json({ status: "success", data: data });
  } catch (e) {
    console.error(e);
    res.status(500).json({ errorMsg: "에러발생." });
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.getComments = async (req, res) => {
  if (typeof req.params.id === "undefined") {
    res.status(400).end();
    return;
  }
  try {
    const video = await Video.query().findById(req.params.id);

    if (!video) {
      res.status(404).json({ status: "novideo" });
      return;
    }

    if (!video.commentAllowed()) {
      return [];
    }

    const query = video.$relatedQuery("VideoComment");
    if (typeof req.query.paging !== "undefined") {
      query.limit(COMMENT_PAGING);
      query.offset((Number(req.query.paging) - 1) * COMMENT_PAGING);
    }

    const comments = await query;

    const json = comments.map((val) => {
      return val.toPublicJSON();
    });

    res.json(json);
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.addComment = async (req, res) => {
  if (
    typeof req.body.comment === "undefined" ||
    typeof req.body.videoId === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }

  try {
    const video = await Video.query().findById(req.body.videoId);

    const inserted = await video.addComment(req.user, {
      comment: req.body.comment,
      parentId: req.body.parentId,
    });

    res.json({ status: "success", data: inserted.toPublicJSON() });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

module.exports.deleteComment = async (req, res) => {
  if (typeof req.params.commentId === "undefined") {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }

  try {
    const result = await deleteVideoComment(req.params.commentid, req.user);

    if (result === true) {
      res.json({ status: "success" });
    } else {
      res.json({ errorMsg: result });
    }
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.updateVideoInfo = async (req, res) => {
  if (typeof req.body.videoId === "undefined") {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }

  try {
    const video = await Video.query().findById(req.body.videoId);

    if (req.user.id !== video.userId && !req.user.isAdmin()) {
      res.status(403).json({ errorMsg: "no permission" });
      return;
    }

    await video.updateInfo(req.body);

    res.json({ status: "success" });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

module.exports.blockVideo = async (req, res) => {
  if (
    typeof req.body.videoId === "undefined" ||
    typeof req.body.type === "undefined" ||
    typeof req.body.reason === "undefined"
  ) {
    res.status(400).json({ status: "invalid params" });
    return;
  }
  if (!req.user.isAdmin()) {
    res.status(403).json({ status: "no permission" });
    return;
  }

  try {
    const video = await Video.query().findById(req.body.videoId);

    if (!video) {
      res.status(404).json({ status: "not exists" });
      return;
    }

    await video
      .$query()
      .patch({ isBlocked: req.body.type, blockComment: req.body.reason });
    res.json({ status: "success" });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
module.exports.getVideoMasterPlaylist = async (req, res) => {
  try {
    const vid = await Video.query().findById(req.params.id);
    if (!vid) {
      res.status(404).json({ status: "novideo" });
      return;
    }
    const playlist = await createMasterPlaylist(vid);

    res.type("application/x-mpegURL");
    res.send(playlist);
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

module.exports.deleteVideo = async (req, res) => {
  try {
    const video = await getVideoById(req.params.id, req.user);

    // 권한에 따라 리턴
    if (
      req.user.isAdmin() ||
      (isLogined(req) && req.user.id === video.userId)
    ) {
      await video.$query().delete();
      res.json({ status: "success" });
    } else {
      res.status(400).json({
        status: "nopermission",
      });
    }
  } catch (e) {
    console.error(e);
    res.status(500).end();
    res.json({ errorMsg: "err" });
  }
};
