const Video = require("../models/video");

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
module.exports.getUserVideoList = async (req, res) => {
  let where = { userId: req.user.id };
  if (req.body.title) {
    where = { ...where, title: req.body.title };
  }
  if (req.body.visibleStatus) {
    where = { ...where, title: req.body.title };
  }

  const videos = await Video.query().where(where);

  const result = videos.map((val) => val.toOwnerJSON());

  res.json({ status: "success", data: result });
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 */
module.exports.getVideoEncodeProgress = async (req, res) => {
  if (typeof req.body.videoId === "undefined") {
    res.status(400).json({ errorMsg: "invalid videoId" });
    return;
  }

  const video = (
    await Video.query().where({
      id: req.body.videoId,
      userId: req.user.id,
    })
  )[0];

  if (!(video instanceof Video)) {
    res.status(404).json({ errorMsg: "no videoId" });
  }

  const queues = await video.getEncodeStatus();

  if (queues === true) {
    res.json({
      status: "success",
      isReady: true,
    });
    return;
  }

  res.json({
    status: "success",
    isReady: false,
    data: queues.map((val) => val.toOwnerJSON()),
  });
};
