const Userlib = require("../lib/User");
const config = require("../config");
const User = require("../models/User");
const { handleRouterError } = require("../lib/router");
/**
 *
 * @param {import("express").Request} req req
 * @param {import("express").Response} res res
 */
module.exports.login = async (req, res) => {
  if (typeof req.body.email === "undefined") {
    res
      .status(400)
      .json({ status: "noemail", errorMsg: "이메일을 입력해주세요" });
    return;
  }
  if (typeof req.body.password === "undefined") {
    res
      .status(400)
      .json({ status: "nopassword", errorMsg: "비밀번호를 입력해주세요." });
    return;
  }
  try {
    let user = await User.query().where("email", req.body.email);

    if (user.length === 0) {
      res.status(401).json({ status: "loginfail", errorMsg: "로그인 실패" });
      return;
    } else {
      user = user[0];
      const token = await user.createLoginToken();
      req.session.userid = user.id;
      req.user = user;
      res.cookie("token", token);
      res.json({
        status: "success",
        data: user.toOwnerJSON(),
        token: token,
      });
    }
  } catch (e) {
    console.error(e);
    throw e;
  }
};

/**
 *
 * @param {import("express").Request} req req
 * @param {import("express").Response} res res
 */
module.exports.logout = async (req, res) => {
  try {
    req.session.destroy((err) => {
      if (err) throw err;
      delete req.user;
      res.json({ status: "success" });
    });
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
};

/**
 *
 * @param {import("express").Request} req req
 * @param {import("express").Response} res res
 */
module.exports.createUser = async (req, res) => {
  if (typeof req.body.email === "undefined") {
    res.status(400).json({ errorMsg: "이메일을 입력해주세요" });
    return;
  }
  if (typeof req.body.password === "undefined") {
    res.status(400).json({ errorMsg: "비밀번호를 입력해주세요" });
    return;
  }
  if (typeof req.body.name === "undefined") {
    res.status(400).json({ errorMsg: "닉네임을 입력해주세요" });
    return;
  }

  try {
    const result = await Userlib.createUser({
      email: req.body.email,
      password: req.body.password,
      name: req.body.name,
    });

    if (result === true) {
      res.json({ status: "success" });
    } else {
      res.status(400).json({ errorMsg: result });
    }
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

/**
 *
 * @param {import("express").Request} req req
 * @param {import("express").Response} res res
 *
 * @description 자기 유저 정보가져오기
 */
module.exports.getMyInfo = async (req, res) => {
  try {
    if (Userlib.isLogined(req)) {
      res.json({ status: "logined", data: req.user.toOwnerJSON() });
    } else {
      res.json({ status: "notlogined" });
    }
  } catch (e) {
    console.error(e);
    res.status(500).json({ errorMsg: "unhandled error" });
  }
};
