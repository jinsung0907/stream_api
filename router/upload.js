const { handleRouterError } = require("../lib/router");
const { uploadSubtitle } = require("../lib/subtitle");
const { uploadVideo } = require("../lib/upload");
const { isLogined } = require("../lib/User");
const Video = require("../models/Video");

module.exports.upload = async (req, res) => {
  if (!isLogined(req)) {
    res.status(401).json({ errorMsg: "로그인이 필요합니다." });
    return;
  }
  try {
    const result = await uploadVideo({
      file: req.files.video,
      info: {
        title: req.body.title,
        description: req.body.description,
        visible: req.body.visible,
      },
      user: req.user,
    });

    if (typeof req.files.subtitle !== "undefined") {
      await uploadSubtitle({
        video: result,
        user: req.user,
        file: req.files.subtitle,
        metadata: {
          title: req.body.subtitleTitle,
          description: req.body.subtitleDescription,
        },
      });
    }
    res.json({
      status: "success",
      data: result.toOwnerJSON(),
    });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.uploadSubtitle = async (req, res) => {
  if (typeof req.body.videoId === "undefined") {
    res.status(400).json({ errorMsg: "invalid videoId" });
    return;
  }

  try {
    const video = await Video.query().findById(req.body.videoId);
    if (typeof video === "undefined" || video === null) {
      res.json({ errorMsg: "존재하지 않는 영상ID" });
      return;
    }

    const subtitle = await uploadSubtitle({
      video: video,
      user: req.user,
      file: req.files.subtitle,
      metadata: {
        title: req.body.title,
        description: req.body.description,
      },
    });

    res.json({ status: "success", data: subtitle.id });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};
