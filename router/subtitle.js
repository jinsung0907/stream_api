const { raw } = require("objection");
const Subtitle = require("../models/Subtitle");
const Video = require("../models/Video");

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.assignToMain = async (req, res) => {
  if (
    typeof req.body.subtitleId === "undefined" ||
    typeof req.body.videoId === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }

  try {
    const video = await Video.query().findById(req.body.videoId);

    if (video.userId !== req.user.id && !req.user.isAdmin()) {
      res.status(403).json({ errorMsg: "no permission" });
      return;
    }

    const subtitle = await Subtitle.query().where({
      id: req.body.subtitleId,
      videoId: video.id,
    });
    await subtitle.makeMain(true);

    res.json({ status: "success" });
  } catch (e) {
    console.error(e);
    res.status(500).json({ errorMsg: "에러발생." });
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.vote = async (req, res) => {
  if (
    typeof req.body.subtitleId === "undefined" ||
    typeof req.body.type === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }

  try {
    const subtitle = await Subtitle.query().findById(req.body.subtitleId);

    let result;
    if (req.body.type) {
      result = await subtitle.upVote(req.user);
    } else {
      result = await subtitle.downVote(req.user);
    }

    res.json({ status: "success", data: result });
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @returns
 */
module.exports.getSubtitleByVideoId = async (req, res) => {
  if (typeof req.params.id === "undefined") {
    res.status(400).json({ errorMsg: "invalid param" });
    return;
  }
  try {
    const subs = await Subtitle.query()
      .select("*", raw("upvote - downvote as score"))
      .where("videoId", req.params.id)
      .orderBy("score", "desc");
    if (subs.length === 0) {
      res.json([]);
      return;
    }

    const json = subs.map((val) => val.toPublicJSON());
    res.json(json);
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
};

module.exports.deleteSubtitle = async (req, res) => {
  if (typeof req.body.subtitleId === "undefined") {
    res.json(400).json({ errorMsg: "invalid params" });
    return;
  }

  try {
    const subtitle = await Subtitle.query().where({
      id: req.body.subtitleId,
      userId: req.user.id,
    });
    if (
      typeof subtitle === "undefined" ||
      subtitle === null ||
      subtitle.length === 0
    ) {
      res.json({ errorMsg: "존재하지 않거나 접근할 수 없는 자막입니다." });
      return;
    }
    await subtitle[0].$query().delete();
    res.json({ status: "success" });
  } catch (e) {
    console.error(e);
    res.status(500).end();
  }
};
