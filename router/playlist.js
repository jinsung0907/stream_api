const { isValidVisibleStatus } = require("../lib/common");
const CustomError = require("../lib/CustomError");
const { createPlayList, playlistById } = require("../lib/playlist");
const { handleRouterError } = require("../lib/router");
const { getVideoById } = require("../lib/video");
const Playlist = require("../models/PlayList");
const PlaylistItem = require("../models/PlayListItem");
const Video = require("../models/Video");

module.exports.getPlayListById = async (req, res) => {
  if (typeof req.params.id === "undefined") {
    res.status(403);
    res.json({ errorMsg: "ID가 없습니다." });
    return;
  }
  try {
    const playlist = await playlistById(req.params.id);

    if (playlist === null) {
      res.status(404).json({
        errorMsg: "존재하지 않는 플레이리스트",
      });
      return;
    }

    await playlist.getItems();
    for (const i in playlist.items) {
      const item = playlist.items[i];
      const video = await getVideoById(item.videoId, req.user);
      item.videoInfo = video.toPublicJSON();
    }

    res.json(playlist.toJSON());
  } catch (e) {
    console.error(e);
    res.status(500).end();
    res.json({ errorMsg: "err" });
  }
};

/**
 *
 * @param {{ body: { paging: number } }} req express request object
 * @param {*} res res
 */
module.exports.listPlaylist = async (req, res) => {
  let paging = req.params.paging;
  if (typeof paging === "undefined" || paging <= 0) {
    paging = 1;
  }

  try {
    const playlists = await Playlist.query()
      .where({ visibleStatus: "public" })
      .limit(20)
      .offset((paging - 1) * 20);

    const jsons = [];

    for (const i in playlists) {
      const playlist = playlists[i];
      await playlist.getFirstItem();
      jsons.push(playlist.toPublicJSON());
    }

    res.json({ status: "success", data: jsons });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

module.exports.getUserPlaylist = async (req, res) => {
  try {
    const list = await Playlist.query().where("userId", req.user.id);

    // videoId 값이 같이 넘어오면 해당 영상의 플레이리스트 추가 여부를 가져옴
    const addlist = await PlaylistItem.query().where({
      userId: req.user.id,
      videoId: req.query.videoId,
    });

    const json = list.map((val) => {
      const res = val.toPublicJSON();

      for (const i in addlist) {
        const item = addlist[i];
        if (val.id === item.playlistId) {
          res.isAdded = item.id;
          break;
        }
      }

      return res;
    });

    res.json({
      status: "success",
      data: json,
    });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

module.exports.addItem = async (req, res) => {
  if (
    typeof req.body.videoId === "undefined" ||
    typeof req.body.playlistId === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid params" });
    return;
  }

  const playlist = await Playlist.query().select().where({
    id: req.body.playlistId,
    userId: req.user.id,
  });
  const video = await Video.query().select().where({
    id: req.body.videoId,
    userId: req.user.id,
  });

  const inserted = await playlist[0].pushItem(video[0]);
  res.json({ status: "success", data: inserted.toJSON() });
};

module.exports.deleteItem = async (req, res) => {
  if (typeof req.params.id === "undefined") {
    res.status(400).json({ errorMsg: "invalid params" });
    return;
  }

  const item = await PlaylistItem.query().select().where({
    id: req.params.id,
    userId: req.user.id,
  });

  const playlist = await item[0].$relatedQuery("Playlist").select();

  await playlist.deleteItem(item[0]);
  res.json({ status: "success" });
};

module.exports.orderItem = async (req, res) => {
  if (
    typeof req.body.itemId === "undefined" ||
    typeof req.body.playlistId === "undefined" ||
    typeof req.body.sequence === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid params" });
    return;
  }

  try {
    const playlist = await Playlist.query().select().where({
      id: req.body.playlistId,
      userId: req.user.id,
    });
    const item = await PlaylistItem.query().select().where({
      id: req.body.itemId,
      playlistId: playlist.id,
      userId: req.user.id,
    });

    await playlist[0].reOrderItem(item[0], req.body.sequence);

    res.json({ status: "success" });
  } catch (e) {
    console.error(e);
    res.status(500);
  }
};

module.exports.editInfo = async (req, res) => {
  if (
    typeof req.body.playlistId === "undefined" ||
    typeof req.body.info === "undefined"
  ) {
    res.status(400).json({ errorMsg: "invalid params" });
    return;
  }

  try {
    const playlist = await Playlist.query().where({
      id: req.body.playlistId,
      userId: req.user.id,
    });

    await playlist[0].updateInfo(req.body.info);

    res.json({ status: "success" });
  } catch (e) {
    console.error(e);
    res.status(500);
  }
};

/**
 *
 * @param {{ body: { title: string, description: string, visibleStatus: string }}} req
 * @param {*} res
 * @returns
 */
module.exports.createPlayList = async (req, res) => {
  try {
    if (typeof req.body.title === "undefined") {
      throw new CustomError("invalidinput", 400, "제목을 입력해주세요.");
    }
    if (typeof req.body.description === "undefined") {
      throw new CustomError("invalidinput", 400, "설명을 입력해주세요.");
    }
    if (
      typeof req.body.visibleStatus === "undefined" ||
      !isValidVisibleStatus(req.body.visibleStatus)
    ) {
      throw new CustomError("invalidinput", 400, "invalid visibleStatus");
    }

    const info = {
      title: req.body.title,
      description: req.body.description,
      visibleStatus: req.body.visibleStatus,
    };
    const created = await createPlayList(info, undefined, req.user);

    res.json({ status: "success", data: created.toPublicJSON() });
  } catch (e) {
    handleRouterError(req, res, e);
  }
};

module.exports.deletePlayList = async (req, res) => {
  if (typeof req.body.playlistId === "undefined") {
    res.status(400).json({ errorMsg: "invalid params" });
    return;
  }

  try {
    const playlist = await Playlist.query().findById(req.body.playlistId);

    if (!playlist) {
      res.status(404).json({ errorMsg: "not exists" });
      return;
    }

    if (!req.user.isAdmin() && playlist.userId !== req.user.id) {
      res.status(403).json({ errorMsg: "no permission" });
      return;
    }

    if (playlist.visibleStatus === "blocked") {
      res.status(403).json({ errorMsg: "no permission(blocked)" });
      return;
    }

    await playlist.$query().delete();
    res.json({ status: "success" });
  } catch (e) {
    console.error(e);
    res.status(500);
  }
};
