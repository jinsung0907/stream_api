const router = require("express").Router();
const playlist = require("./playlist");
const upload = require("./upload");
const user = require("./user");
const video = require("./video");
const subtitle = require("./subtitle");

const search = require("./search");
const studio = require("./studio");
const { isLogined } = require("../lib/User");

const requireLogin = (req, res, next) => {
  if (isLogined(req)) {
    next();
  } else {
    res
      .status(403)
      .json({ status: "requireLogin", errorMsg: "로그인이 필요합니다." });
  }
};

router.get("/video/list", video.listVideo);
router.get("/video/list/:paging", video.listVideo);
router.post("/video/comment", requireLogin, video.addComment);
router.delete("/video/comment/:commentId", requireLogin, video.deleteComment);

router.get("/video/:id/subtitle", subtitle.getSubtitleByVideoId);
router.get("/video/:id/comments", video.getComments);
router.get("/video/:id/view", video.viewCountVideo);
router.get("/video/:id/playlist", video.getVideoMasterPlaylist);
router.get("/video/:id", video.videoById);
router.delete("/video/:id", requireLogin, video.deleteVideo);

router.post("/video/like", requireLogin, video.likeVote);
router.post("/video/edit", requireLogin, video.updateVideoInfo);

router.post("/search", search.searchVideo);

router.get("/playlist/list/", playlist.listPlaylist);
router.get("/playlist/list/:paging", playlist.listPlaylist);
router.get("/playlist/mylists", requireLogin, playlist.getUserPlaylist);
router.get("/playlist/:id", playlist.getPlayListById);
router.delete("/playlist", requireLogin, playlist.deletePlayList);

router.post("/playlist/create", requireLogin, playlist.createPlayList);
router.post("/playlist/edit", requireLogin, playlist.editInfo);
router.post("/playlist/item/add", requireLogin, playlist.addItem);
router.post("/playlist/item/edit", requireLogin, playlist.orderItem);
router.delete("/playlist/item/:id", requireLogin, playlist.deleteItem);

router.post("/subtitle/upload", requireLogin, upload.uploadSubtitle);
router.post("/subtitle/setmain", requireLogin, subtitle.assignToMain);
router.post("/subtitle/vote", requireLogin, subtitle.vote);

router.post("/login", user.login);
router.get("/logout", requireLogin, user.logout);
router.post("/register", user.createUser);
router.get("/user/myinfo", user.getMyInfo);

router.post("/upload", requireLogin, upload.upload);

router.post("/studio/mylist", requireLogin, studio.getUserVideoList);
router.post(
  "/studio/videoEncodeStatus",
  requireLogin,
  studio.getVideoEncodeProgress
);

module.exports = router;
