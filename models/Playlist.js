const { Model, raw } = require("objection");
const { getTimeStamp, isValidVisibleStatus } = require("../lib/common");
const CustomError = require("../lib/CustomError");
const PlaylistItem = require("./PlaylistItem");

module.exports = class Playlist extends Model {
  static get tableName() {
    return "Playlist";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["title", "visibleStatus", "createdDate"],

      properties: {
        id: { type: "string" }, // 6자리 (영/숫자 조합 (대소문자 구별))
        title: { type: "string" },
        description: { type: "string" },
        visibleStatus: { type: "string" }, // public, invisible, private
        userId: { type: "string" },
        userName: { type: "string" },
        itemCount: { type: "integer", default: 0 },
        createdDate: { type: "integer" },
        lastUpdateDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    return {
      PlaylistItem: {
        relation: Model.HasManyRelation,
        modelClass: PlaylistItem,
        join: {
          from: "Playlist.id",
          to: "PlaylistItem.playlistId",
        },
      },
    };
  }

  static async beforeDelete({ items }) {
    const promises = items.map(
      (item) =>
        new Promise((resolve, reject) => {
          PlaylistItem.query()
            .delete()
            .where({
              playlistId: item.id,
            })
            .then(() => resolve());
        })
    );
    await Promise.all(promises);
  }

  async getItems() {
    this.items = await this.$relatedQuery("PlaylistItem").orderBy(
      "sequence",
      false
    );

    return this.items;
  }

  async getFirstItem() {
    const item = (
      await this.$relatedQuery("PlaylistItem")
        .orderBy("sequence", false)
        .limit(1)
    )[0];

    this.firstItem = await item.$relatedQuery("Video");

    return this.firstItem;
  }

  /**
   *
   * @param {Video} video
   * @returns {PlaylistItem}
   */
  async pushItem(video) {
    const lastnumber = await this.$relatedQuery("PlaylistItem")
      .select()
      .orderBy("sequence", "desc")
      .limit(1);

    const inserted = await this.$relatedQuery("PlaylistItem").insert({
      videoId: video.id,
      userId: this.userId,
      playlistId: this.id,
      sequence: lastnumber.length === 0 ? 1 : lastnumber[0].sequence + 1,
    });

    await this.$query().patch({
      itemCount: raw("itemCount + 1"),
      lastUpdateDate: getTimeStamp(),
    });

    return inserted;
  }

  /**
   *
   * @param {PlaylistItem} item
   */
  async deleteItem(item) {
    await item.$query().delete();
    await this.$query().patch({
      itemCount: raw("itemCount - 1"),
      lastUpdateDate: getTimeStamp(),
    });

    this.alignItems();
  }

  async alignItems() {
    let items = await this.$relatedQuery("PlaylistItem")
      .select()
      .orderBy("sequence");

    items = items.map(
      (val, i) =>
        new Promise((resolve, reject) => {
          val
            .$query()
            .patch({
              sequence: Number(i) + 1,
            })
            .then(() => {
              resolve();
            });
        })
    );

    await Promise.all(items);
  }

  /**
   *
   * @param {PlaylistItem} item
   * @param {*} sequence
   */
  async reOrderItem(item, sequence) {
    let items = await this.$relatedQuery("PlaylistItem")
      .select()
      .where("sequence", ">=", sequence);

    items = items.map(
      (val, i) =>
        new Promise((resolve, reject) => {
          val
            .$query()
            .patch({
              sequence: raw("sequence + 1"),
            })
            .then(() => {
              resolve();
            });
        })
    );

    await Promise.all(items);

    await item.$query().patch({ sequence: sequence });
    await this.$query().patch({ lastUpdateDate: getTimeStamp() });

    await this.alignItems();
  }

  async updateInfo(info) {
    const patch = {};
    if (info.title) patch.title = info.title;
    if (info.description) patch.description = info.description;
    if (info.visibleStatus) {
      if (!isValidVisibleStatus(info.visibleStatus)) {
        throw new CustomError("Invalidinput", 400, "잘못된 공개상태 값입니다.");
      }
      patch.visibleStatus = info.visibleStatus;
    }

    patch.lastUpdateDate = getTimeStamp();
    const updated = await this.$query().patchAndFetch(patch);

    return updated;
  }

  toPublicJSON() {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      visibleStatus: this.visibleStatus,
      userId: this.userId,
      userName: this.userName,
      itemCount: this.itemCount,
      createdDate: this.createdDate,
      lastUpdateDate: this.lastUpdateDate,
      items: this.items && this.items.map((val) => val.toJSON()),
      firstItem: this.firstItem && this.firstItem.toPublicJSON(),
    };
  }
};
