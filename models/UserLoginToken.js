const { Model } = require("objection");

module.exports = class UserLoginToken extends Model {
  static get tableName() {
    return "UserLoginToken";
  }

  static get jsonSchema() {
    return {
      type: "object",
      // required: ['firstName', 'lastName'],

      properties: {
        userId: { type: "string" },
        token: { type: "string" },
        loginDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    const User = require("./User");
    return {
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "UserLoginToken.userId",
          to: "User.id",
        },
      },
    };
  }
};
