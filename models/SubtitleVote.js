const { Model } = require("objection");
const Subtitle = require("./subtitle");

module.exports = class SubtitleVote extends Model {
  static get tableName() {
    return "SubtitleVote";
  }

  static get jsonSchema() {
    return {
      type: "object",
      // required: ['firstName', 'lastName'],

      properties: {
        id: { type: "integer" },
        userId: { type: "string" },
        subtitleId: { type: "integer" },
        type: { type: "boolean" },
        insertDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Subtitle,
        join: {
          from: "SubtitleVote.subtitleId",
          to: "Subtitle.id",
        },
      },
    };
  }
};
