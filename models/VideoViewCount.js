const { Model } = require("objection");
const User = require("./User");
const Video = require("./Video");

module.exports = class VideoViewCount extends Model {
  static get tableName() {
    return "VideoViewCount";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["date"],

      properties: {
        id: { type: "integer" },
        videoId: { type: "string" },
        ip: { type: ["string", "null"], default: null },
        userId: { type: ["string", "null"], default: null },
        date: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: "VideoViewCount.videoId",
          to: "Video.id",
        },
      },
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "VideoViewCount.userId",
          to: "User.id",
        },
      },
    };
  }
};
