const { Model } = require("objection");
const User = require("./User");
const Video = require("./Video");

module.exports = class VideoComment extends Model {
  static get tableName() {
    return "VideoComment";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["userId", "comment", "insertDate"],

      properties: {
        id: { type: "integer" },
        videoId: { type: "string" },
        parentId: { type: ["integer", null], default: null },
        userId: { type: "string" },
        userName: { type: "string" },
        comment: { type: "text" },

        commentCount: { type: "integer", default: 0 },
        likeCount: { type: "integer", default: 0 },
        dislikeCount: { type: "integer", default: 0 },

        insertDate: { type: "integer" },
        lastUpdateDate: { type: ["integer", null], default: null },
      },
    };
  }

  static get relationMappings() {
    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: "VideoComment.videoId",
          to: "Video.id",
        },
      },
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "VideoComment.userId",
          to: "User.id",
        },
      },
    };
  }

  toPublicJSON() {
    const json = this.toJSON();
    delete json.id;
    delete json.dislikeCount;

    return json;
  }
};
