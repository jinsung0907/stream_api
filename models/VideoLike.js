const { Model } = require("objection");
const User = require("./User");
const Video = require("./Video");

module.exports = class VideoLike extends Model {
  static get tableName() {
    return "VideoLike";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["type", "insertDate"],

      properties: {
        id: { type: "integer" },
        videoId: { type: "string" },
        userId: { type: "string" },
        type: { type: "boolean" }, // true = like, false = dislike
        insertDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: "VideoLike.videoId",
          to: "Video.id",
        },
      },
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "VideoLike.userId",
          to: "User.id",
        },
      },
    };
  }
};
