const { Model, raw } = require("objection");
const { getTimeStamp } = require("../lib/common");
const User = require("./User");
const VideoComment = require("./VideoComment");
const VideoLike = require("./VideoLike");
const VideoTag = require("./VideoTag");
const VideoViewCount = require("./VideoViewCount");
const { VIEWCOUNT_CHECK_TIME } = require("../lib/constants");
const Subtitle = require("./Subtitle");
const CustomError = require("../lib/CustomError");
const EncodeQueue = require("./EncodeQueue");

module.exports = class Video extends Model {
  static get tableName() {
    return "Video";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: [
        "originalFileName",
        "title",
        "description",
        "userId",
        "userName",
        "visibleStatus",
        "isReady",
        "uploadDate",
      ],

      properties: {
        id: { type: "string" }, // 6자리 (영/숫자 조합 (대소문자 구별))
        originalFileName: { type: "string" },
        title: { type: "string" }, // 제목,
        description: { type: "text" },
        userId: { type: "string" },
        userName: { type: "string" },

        width: { type: "integer" },
        height: { type: "integer" },
        encodeWidth: { type: "integer" },
        encodeHeight: { type: "integer" },
        videoLength: { type: "integer" },
        fileHash: { type: "string" },

        viewCount: { type: "integer", default: 0 },
        likeCount: { type: "integer", default: 0 },
        dislikeCount: { type: "integer", default: 0 },
        commentCount: { type: "integer", default: 0 },

        visibleStatus: { type: "string", default: "private" }, // 비디오 공개상태 public: 전체공개, unlisted: 미공개, private: 비공개
        isReady: { type: "boolean", default: false }, // 영상이 준비되었는지 여부 (인코딩 등)
        isBlocked: { type: "boolean", default: false }, // 영상 차단시
        blockComment: { type: ["text", "null"], default: null }, // 영상 차단시 코멘트

        uploadDate: { type: "integer" },
        readyDate: { type: ["integer", "null"], default: null },
        publicDate: { type: ["integer", "null"], default: null },

        uploadError: { type: ["string", "null"], default: null }, // 에러시 입력, 아니면 입력자체를 안함
      },
    };
  }

  static get relationMappings() {
    return {
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "Video.userId",
          to: "User.id",
        },
      },
      VideoTag: {
        relation: Model.HasManyRelation,
        modelClass: VideoTag,
        join: {
          from: "Video.id",
          to: "VideoTag.videoId",
        },
      },
      VideoComment: {
        relation: Model.HasManyRelation,
        modelClass: VideoComment,
        join: {
          from: "Video.id",
          to: "VideoComment.videoId",
        },
      },
      VideoLike: {
        relation: Model.HasManyRelation,
        modelClass: VideoLike,
        join: {
          from: "Video.id",
          to: "VideoLike.videoId",
        },
      },
      VideoViewCount: {
        relation: Model.HasManyRelation,
        modelClass: VideoViewCount,
        join: {
          from: "Video.id",
          to: "VideoViewCount.videoId",
        },
      },
      Subtitle: {
        relation: Model.HasManyRelation,
        modelClass: Subtitle,
        join: {
          from: "Video.id",
          to: "Subtitle.videoId",
        },
      },
      EncodeQueue: {
        relation: Model.HasManyRelation,
        modelClass: EncodeQueue,
        join: {
          from: "Video.id",
          to: "EncodeQueue.videoId",
        },
      },
    };
  }

  /*
    Video Record 삭제시 연관된 것 모두 삭제
     - 영상 파일 (원본, 인코딩, 썸네일)
     - 댓글
     - 자막 (파일까지)
     - 자막 투표
     - 좋아요 투표
     - 태그
     - 조회수
  */
  static async beforeDelete({ asFindQuery, cancelQuery }) {
    const videos = await asFindQuery();
    const { deleteVideoFiles } = require("../lib/video");

    for (const i in videos) {
      const vid = videos[i];
      await deleteVideoFiles(vid);
      await vid.$relatedQuery("VideoComment").delete();
      await vid.$relatedQuery("Subtitle").delete();
      await vid.$relatedQuery("VideoLike").delete();
      await vid.$relatedQuery("VideoTag").delete();
      await vid.$relatedQuery("VideoViewCount").delete();
    }
  }

  // 공개용 정보
  toPublicJSON() {
    const json = {
      id: this.id,
      title: this.title,
      description: this.description,
      userId: this.userId,
      userName: this.userName,
      videoLength: this.videoLength,
      publicDate: this.publicDate,
      viewCount: this.viewCount,
      likeCount: this.likeCount,
      dislikeCount: this.dislikeCount,
      commentCount: this.commentCount,
    };

    /*
    if (this.tags) {
      json.tags = this.tags;
    }
    */

    return json;
  }

  // 영상 업로더용 정보
  toOwnerJSON() {
    const json = this.toPublicJSON();

    json.originalFileName = this.originalFileName;
    json.visibleStatus = this.visibleStatus;
    json.isReady = this.isReady;
    json.isBlocked = this.isBlocked;
    json.blockComment = this.blockComment;
    json.uploadDate = this.uploadDate;
    json.readyDate = this.readyDate;
    json.uploadError = this.uploadError;

    return json;
  }

  async getTags() {
    const tags = await this.$relatedQuery("VideoTag").select();

    this.tags = tags.map((val) => {
      return val.value;
    });

    return this.tags;
  }

  /**
   * @description 영상에 댓글 게시가능여부
   * @returns {boolean} returns true if can comment if not false
   */
  commentAllowed() {
    // 공개 동영상, 미공개 영상만 댓글 가능
    if (this.visibleStatus === "public" || this.visibleStatus === "unlisted") {
      return true;
    } else {
      return false;
    }
  }

  /**
   *
   * @param {User} user user Model
   *
   * @returns {{ like: number, dislike: number}} like, dislike 변동량 리턴
   */
  async upLike(user) {
    if (!(user instanceof User)) {
      throw new Error("invalid user model");
    }

    try {
      // 좋아요 조회하여 있으면 무시, 있는데 싫어요면 좋아요로 변경, 없으면 입력
      const like = await this.$relatedQuery("VideoLike")
        .select()
        .where("userId", user.id);

      // 조회되지 않으면 입력
      if (like.length === 0) {
        await this.$relatedQuery("VideoLike").insert({
          userId: user.id,
          type: true,
          insertDate: Math.floor(Date.now() / 1000),
        });
        await this.$query().patch({ likeCount: raw("likeCount + 1") });
        return { like: 1, dislike: 0 };
      }
      // 조회될시
      else {
        // 싫어요면 좋아요로 바꾸기
        if (like[0].type === false) {
          await like[0]
            .$query()
            .patch({ type: true, insertDate: Math.floor(Date.now() / 1000) });
          await this.$query().patch({
            likeCount: raw("likeCount + 1"),
            dislikeCount: raw("dislikeCount - 1"),
          });
          return { like: 1, dislike: -1 };
        }
        // 좋아요면 삭제
        else {
          await like[0].$query().delete();
          await this.$query().patch({
            likeCount: raw("likeCount - 1"),
          });
          return { like: -1, dislike: 0 };
        }
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   *
   * @param {User} user user model
   *
   * @returns {{ like: number, dislike: number}} like, dislike 변동량 리턴
   */
  async upDislike(user) {
    if (!(user instanceof User)) {
      throw new Error("invalid user model");
    }

    try {
      // uplike의 반대로 작동
      const like = await this.$relatedQuery("VideoLike")
        .select()
        .where("userId", user.id);

      // 조회되지 않으면 입력
      if (like.length === 0) {
        await this.$relatedQuery("VideoLike").insert({
          userId: user.id,
          type: false,
          insertDate: Math.floor(Date.now() / 1000),
        });
        await this.$query().patch({ dislikeCount: raw("dislikeCount + 1") });
        return { like: 0, dislike: 1 };
      }
      // 조회될시
      else {
        // 좋아요면 싫어요로 변경
        if (like[0].type === true) {
          await like[0]
            .$query()
            .patch({ type: false, insertDate: Math.floor(Date.now() / 1000) });
          await this.$query().patch({
            dislikeCount: raw("dislikeCount + 1"),
            likeCount: raw("likeCount - 1"),
          });
          return { like: -1, dislike: 1 };
        }
        // 싫어요면 삭제
        else {
          await like[0].$query().delete();
          await this.$query().patch({
            dislikeCount: raw("dislikeCount - 1"),
          });
          return { like: 0, dislike: -1 };
        }
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   *
   * @param {{ ip: string, user: User }} param0 ip, user
   *
   * @returns {boolean} return true
   */
  async viewCountUp({ ip, user }) {
    if (typeof ip === "undefined") {
      throw new Error("session is not set");
    }

    try {
      // user가 없으면 ip로 입력, 있으면 user로 입력
      const query = this.$relatedQuery("VideoViewCount")
        .select()
        .where("date", ">", getTimeStamp() - VIEWCOUNT_CHECK_TIME);
      const insertValue = {
        date: getTimeStamp(),
      };
      if (typeof user === "undefined" || !(user instanceof User)) {
        query.where("ip", ip);
        insertValue.ip = ip;
      } else {
        query.where("userId", user.id);
        insertValue.userId = user.id;
      }
      const result = await query;

      // 중복 결과가 없으면 입력
      if (result.length === 0) {
        await this.$relatedQuery("VideoViewCount").insert(insertValue);
        await this.$query().patch({ viewCount: raw("viewCount + 1") });
      }

      return true;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   *
   * @description block video
   * @param {boolean} block true: block, false: unblock
   * @param {string} comment blocking reason
   *
   * @returns {boolean} Return true if success
   */
  async blockVideo(block, comment) {
    try {
      await this.$query().patch({ isBlocked: block, blockComment: comment });

      return true;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   *
   * @param {User} user
   * @param {{ comment: string, parentId: number }} param1
   *
   * @returns {VideoComment}
   */
  async addComment(user, { comment, parentId }) {
    if (!(user instanceof User)) {
      throw new Error("invalid user model");
    }

    if (typeof comment === "undefined") {
      throw new Error("invalid comment data");
    }
    if (typeof parentId === "undefined") {
      parentId = null;
    }

    try {
      if (!this.commentAllowed()) {
        throw new CustomError(
          "NO_AUTH",
          401,
          "댓글을 달 수 없는 동영상입니다."
        );
      }

      const result = await this.$transaction(async (trx) => {
        const insertdata = {
          comment: comment,
          userId: user.id,
          userName: user.name,
          insertDate: Math.floor(Date.now() / 1000),
        };
        if (typeof parentId !== "undefined") {
          insertdata.parentId = parentId;
        }

        await this.$query(trx).patch({
          commentCount: raw("commentCount + 1"),
        });
        return await this.$relatedQuery("VideoComment", trx).insert(insertdata);
      });
      return result;
    } catch (e) {
      console.error(e);
      throw new Error("err while add comment");
    }
  }

  async updateInfo(info) {
    try {
      const patch = {
        title: info.title,
        description: info.description,
        visibleStatus: info.visibleStatus,
      };

      // 영상 비공개에서 공개로 바꿀시 공개날짜 입력
      if (this.publicDate === null && info.visibleStatus === "public") {
        patch.publicDate = Math.floor(Date.now() / 1000);
      }

      // 태그 업데이트
      if (info.tags) {
        await this.$relatedQuery("VideoTag").delete();
        const tags = info.tags.map((val) => {
          return {
            videoId: this.id,
            value: val,
          };
        });
        await this.$relatedQuery("VideoTag").insert(tags);
      }

      // 정보 업데이트
      const vid = await this.$query().patchAndFetch(patch);

      return vid;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   * @returns {true | Array<EncodeQueue>} Return true if encoding is completed. Object if encoding
   */
  async getEncodeStatus() {
    if (this.isReady === true) {
      return true;
    }
    return await this.$relatedQuery("EncodeQueue");
  }
};
