const { Model } = require("objection");
const Video = require("./Video");

module.exports = class VideoTag extends Model {
  static get tableName() {
    return "VideoTag";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["videoId", "value"],

      properties: {
        id: { type: "integer" },
        videoId: { type: "string" },
        value: { type: "string" },
      },
    };
  }

  static get relationMappings() {
    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: "VideoTag.videoId",
          to: "Video.id",
        },
      },
    };
  }
};
