const { nanoid } = require("nanoid");
const { Model } = require("objection");
const { getTimeStamp } = require("../lib/common");
const UserAttribute = require("./UserAttribute");
const UserLoginToken = require("./UserLoginToken");

module.exports = class User extends Model {
  static get tableName() {
    return "User";
  }

  static get jsonSchema() {
    return {
      type: "object",
      // required: ['firstName', 'lastName'],

      properties: {
        id: { type: "string" },
        name: { type: "string" },
        password: { type: "string" },
        email: { type: "string" },
        emailVerified: { type: ["integer", "null"], default: null }, // 인증 된 시간 입력
        emailVerifyCode: { type: "string" },
        registerDate: { type: "integer" },
        isVirtualUser: { type: "boolean", default: false },
      },
    };
  }

  static get relationMappings() {
    return {
      UserAttribute: {
        relation: Model.HasManyRelation,
        modelClass: UserAttribute,
        join: {
          from: "User.id",
          to: "UserAttribute.userId",
        },
      },
      UserLoginToken: {
        relation: Model.HasManyRelation,
        modelClass: UserLoginToken,
        join: {
          from: "User.id",
          to: "UserLoginToken.userId",
        },
      },
    };
  }

  // 추후 어드민 기능에 대응하기 위함
  // 일단은 하드코딩
  isAdmin() {
    if (this.id === 1) {
      return true;
    }
    return false;
  }

  async getAttributes() {
    try {
      let attrs = await this.$relatedQuery("UserAttribute").select();
      attrs = attrs.map((val) => val.attribute);

      return attrs;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  async checkAttribute(value) {
    if (typeof value === "undefined") {
      throw new Error("invalid attribute value");
    }
    try {
      const attr = await this.$relatedQuery("UserAttribute").select().where({
        attribute: value,
      });

      if (typeof attr === "undefined" || attr === null || attr.length === 0) {
        return false;
      } else {
        return true;
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  async insertAttribute(attr) {
    if (typeof attr === "undefined") {
      throw new Error("invalid attr");
    }
    try {
      await this.$relatedQuery("UserAttribute").insert({
        attribute: attr,
        insertDate: getTimeStamp(),
      });

      return true;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  async createLoginToken() {
    const token = nanoid();

    try {
      await this.$relatedQuery("UserLoginToken").insert({
        token: token,
        loginDate: getTimeStamp(),
      });

      return token;
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  toPublicJSON() {
    return {
      id: this.id,
      name: this.name,
    };
  }

  toOwnerJSON() {
    return {
      id: this.id,
      name: this.name,
      email: this.email,
      emailVerified: this.emailVerified,
      registerDate: this.registerDate,
      isVirtualUser: this.isVirtualUser,
    };
  }
};
