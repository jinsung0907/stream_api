const { Model } = require("objection");
const User = require("./User");

module.exports = class UserAttribute extends Model {
  static get tableName() {
    return "UserAttribute";
  }

  static get jsonSchema() {
    return {
      type: "object",
      // required: ['firstName', 'lastName'],

      properties: {
        userId: { type: "string" },
        attribute: { type: "string" }, // 속성 이름
        value: { type: ["string", "null"], default: null }, // 추가 속성 값
        insertDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    return {
      User: {
        relation: Model.BelongsToOneRelation,
        modelClass: User,
        join: {
          from: "UserAttribute.userId",
          to: "User.id",
        },
      },
    };
  }
};
