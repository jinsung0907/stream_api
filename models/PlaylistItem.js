const { Model } = require("objection");

module.exports = class PlaylistItem extends Model {
  static get tableName() {
    return "PlaylistItem";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["videoId", "playlistId", "sequence"],

      properties: {
        id: { type: "integer" },
        videoId: { type: "string" },
        userId: { type: "string" },
        playlistId: { type: "string" },
        sequence: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    const Playlist = require("./PlayList");
    const Video = require("./Video");

    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: "PlaylistItem.videoId",
          to: "Video.id",
        },
      },
      Playlist: {
        relation: Model.BelongsToOneRelation,
        modelClass: Playlist,
        join: {
          from: "PlaylistItem.playlistId",
          to: "Playlist.id",
        },
      },
    };
  }
};
