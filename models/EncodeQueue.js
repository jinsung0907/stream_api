const { Model } = require("objection");

module.exports = class EncodeQueue extends Model {
  static get tableName() {
    return "EncodeQueue";
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["videoId", "fileName", "insertDate"],

      properties: {
        id: { type: "integer" },
        videoId: { type: "string" },
        fileName: { type: "string" },
        resolution: { type: "integer" },
        status: { type: "string" }, // 진행상태 waiting, queued, running, completed, errored, canceled
        progressText: { type: "string" }, // 진행상황 설명 텍스트
        errorMsg: { type: "string" }, // 에러 메시지 입력
        startDate: { type: ["integer", "null"], default: null },
        endDate: { type: ["integer", "null"], default: null },
        insertDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    const Video = require("./Video");

    return {
      Video: {
        relation: Model.BelongsToOneRelation,
        modelClass: Video,
        join: {
          from: "EncodeQueue.VideoId",
          to: "Video.id",
        },
      },
    };
  }

  toOwnerJSON() {
    return {
      id: this.id,
      videoId: this.videoId,
      resolution: this.resolution,
      status: this.status,
      progressText: this.progressText,
      startDate: this.startDate,
      endDate: this.endDate,
      insertDate: this.insertDate,
    };
  }
};
