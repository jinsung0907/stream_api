const { Model, raw } = require("objection");
const { CDNURL } = require("../config");
const { deleteSubtitleFile } = require("../lib/subtitle");
const SubtitleVote = require("./subtitleVote");
const User = require("./User");

module.exports = class Subtitle extends Model {
  static get tableName() {
    return "Subtitle";
  }

  static get jsonSchema() {
    return {
      type: "object",
      // required: ['firstName', 'lastName'],

      properties: {
        id: { type: "integer" },

        originalFileName: { type: "string" },
        fileHash: { type: "string" },

        videoId: { type: "string" },
        title: { type: "string" },
        description: { type: "text" },
        userId: { type: "string" },
        userName: { type: "string" },

        isMain: { type: "boolean" },
        upvote: { type: "integer", default: 0 },
        downvote: { type: "integer", default: 0 },

        uploadDate: { type: "integer" },
      },
    };
  }

  static get relationMappings() {
    return {
      SubtitleVote: {
        relation: Model.HasManyRelation,
        modelClass: SubtitleVote,
        join: {
          from: "Subtitle.id",
          to: "SubtitleVote.subtitleId",
        },
      },
    };
  }

  // 삭제시 자막 파일, 투표도 같이 삭제
  static async beforeDelete({ asFindQuery }) {
    const Subs = await asFindQuery();

    for (const i in Subs) {
      await Subs[i].$relatedQuery("SubtitleVote").delete();
      await deleteSubtitleFile(Subs[i]);
    }
  }

  toPublicJSON() {
    return {
      id: this.id,
      videoId: this.videoId,
      title: this.title,
      description: this.description,
      userId: this.userId,
      userName: this.userName,
      isMain: this.isMain,
      upvote: this.upvote,
      // downvote: this.downvote
      uploadDate: this.uploadDate,
      src: CDNURL + "/subtitle/" + this.id + ".vtt",
    };
  }

  /**
   *
   * @param {User} user usermodel
   */
  async upVote(user) {
    if (!(user instanceof User)) {
      throw new Error("invalid user model");
    }

    try {
      // 좋아요 조회하여 있으면 무시, 있는데 싫어요면 좋아요로 변경, 없으면 입력
      const like = await this.$relatedQuery("SubtitleVote")
        .select()
        .where("userId", user.id);

      // 조회되지 않으면 입력
      if (like.length === 0) {
        await this.$relatedQuery("SubtitleVote").insert({
          userId: user.id,
          type: true,
          insertDate: Math.floor(Date.now() / 1000),
        });
        await this.$query().patch({ upvote: raw("upvote + 1") });
        return { like: 1, dislike: 0 };
      }
      // 조회될시
      else {
        // 싫어요면 좋아요로 바꾸기
        if (like[0].type === false) {
          await like[0]
            .$query()
            .patch({ type: true, insertDate: Math.floor(Date.now() / 1000) });
          await this.$query().patch({
            upvote: raw("upvote + 1"),
            downvote: raw("downvote - 1"),
          });
          return { like: 1, dislike: -1 };
        }
        // 좋아요면 삭제
        else {
          await like[0].$query().delete();
          await this.$query().patch({
            upvote: raw("upvote - 1"),
          });
          return { like: -1, dislike: 0 };
        }
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   *
   * @param {User} user user model
   *
   * @returns {boolean} returns true
   */
  async downVote(user) {
    if (!(user instanceof User)) {
      throw new Error("invalid user model");
    }

    try {
      // uplike의 반대로 작동
      const like = await this.$relatedQuery("SubtitleVote")
        .select()
        .where("userId", user.id);

      // 조회되지 않으면 입력
      if (like.length === 0) {
        await this.$relatedQuery("SubtitleVote").insert({
          userId: user.id,
          type: false,
          insertDate: Math.floor(Date.now() / 1000),
        });
        await this.$query().patch({ downvote: raw("downvote + 1") });
        return { like: 0, dislike: 1 };
      }
      // 조회될시
      else {
        // 좋아요면 싫어요로 변경
        if (like[0].type === true) {
          await like[0]
            .$query()
            .patch({ type: false, insertDate: Math.floor(Date.now() / 1000) });
          await this.$query().patch({
            downvote: raw("downvote + 1"),
            upvote: raw("upvote - 1"),
          });
          return { like: -1, dislike: 1 };
        }
        // 싫어요면 삭제
        else {
          await like[0].$query().delete();
          await this.$query().patch({
            downvote: raw("downvote - 1"),
          });
          return { like: 0, dislike: -1 };
        }
      }
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  /**
   *
   * @param {boolean} val true on assign, false on discharge
   */
  async makeMain(val) {
    // 현재 메인인것 모두 지움
    await Subtitle.query()
      .patch({
        isMain: false,
      })
      .where({
        videoId: this.videoId,
      });

    // 메인으로 변경
    await this.$query().patch({
      isMain: val,
    });
  }
};
